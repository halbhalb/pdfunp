pdfunp
======

Convert PDF documents made of  2-page spreads into separate pages. 

rationale
---------

There is a lot of great books scanned into PDF files and made available on the internet. 
As a result of scanning process, these PDFs are most often made up of two-page spreads: two 
consecutive pages of the original next to each other make up a single page of the PDF. 
This is not ideal for reading on a e-reader like Kindle. 
This script transforms such PDFs into simpler one with a single book page per a PDF page.

dependencies
------------

The script is built in Python using pyPdf library. The library is available here: 
http://pybrary.net/pyPdf/


On a Ubuntu Linux pyPdf can be installed with this command: **apt-get install python-pypdf**


usage
-----

Run **pdfunp** with no arguments for instructions.

status
------

Basic functionality is implemented. Check issues list for bugs and to do list.